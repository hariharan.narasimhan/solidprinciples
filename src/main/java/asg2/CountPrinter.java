package asg2;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

public class CountPrinter {
    private CharacterCounter characterCounter;
    private String outputFilePath;

    public CountPrinter(CharacterCounter characterCounter, String outputFilePath) {
        this.characterCounter = characterCounter;
        this.outputFilePath = outputFilePath;
    }

    public void writeToFile() throws IOException {
        FileWriter outputFileWriter = null;
        try {
            outputFileWriter = new FileWriter(outputFilePath);
            for(Map.Entry<Character, Integer> charCountEntry:
                    characterCounter.getCharacterCount().entrySet()) {
                outputFileWriter.write(charCountEntry.getKey() + " - " + charCountEntry.getValue() + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(outputFileWriter != null)
                outputFileWriter.close();
        }
    }
}

package asg2;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class CharacterCounter {
    private String inputFilePath;
    private Map<Character, Integer> characterCount;

    public CharacterCounter(String inputFilePath) {
        this.inputFilePath = inputFilePath;
        this.characterCount = new HashMap<>();
    }

    public void countCharacters() throws FileNotFoundException {
        File file = new File(inputFilePath);
        Scanner inputFileScanner = new Scanner(file);
        while(inputFileScanner.hasNextLine()) {
            char[] charArray = inputFileScanner.nextLine().toCharArray();
            for(char character: charArray) {
                int count = 0;
                Integer cachedCount = characterCount.get(character);
                if(cachedCount != null) {
                    count = cachedCount;
                }
                characterCount.put(character, ++count);
            }
        }
    }

    public Map<Character, Integer> getCharacterCount() {
        return characterCount;
    }
}

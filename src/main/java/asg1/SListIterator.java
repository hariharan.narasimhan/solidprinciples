package asg1;

public class SListIterator<T> {
    private Node<T> root;
    private Node<T> current;

    public boolean hasNext() {
        return current != null;
    }

    private void validateCurrentNode() {
        if (current == null)
            throw new RuntimeException("current node is null");
    }

    public T next() {
        validateCurrentNode();
        T data = current.getData();
        current = current.getNext();
        return data;
    }



    public void add(T data) {
        Node temp = root;
        Node<T> next = new Node<>(data);
        if(temp == null)
            root = current = next;
        else {
            while (temp.getNext() != null) {
                temp = temp.getNext();
            }
            temp.setNext(next);
        }
    }
}

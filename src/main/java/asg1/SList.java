package asg1;

public class SList<T>  {
    SListIterator<T> listIterator = null;
    public SListIterator<T> iterator() {
        if(listIterator == null)
            listIterator = new SListIterator<T>();
        return listIterator;
    }

    @Override
    public String toString() {
        StringBuffer returnString = new StringBuffer();
        SListIterator<T> listIterator = iterator();
        while(listIterator.hasNext()) {
            returnString
                    .append("\n")
                    .append(listIterator.next())
                    .append(",");
        }
        return returnString.toString();
    }
}

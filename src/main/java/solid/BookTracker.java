package solid;

/**
 * class to show interface segregation - BookTrackOperations is used just to track the data
 */
public class BookTracker implements BookTrackOperations {
    private Book book;

    public BookTracker(Book book) {
        this.book = book;
    }

    @Override
    public void track(int pageNo) {
        String output = pageNo + " for book " + this.book.getTitle() + " is being tracked";
        System.out.println(output);
    }
}

package solid;

import java.io.IOException;

/**
 * class to show dependency inversion
 * used in BookPrinterTest
 */
public class BookHandler {
    private BookPrintOperations bookPrintOperations;
    private BookTrackOperations bookTrackOperations;

    public BookHandler(BookPrintOperations bookPrintOperations, BookTrackOperations bookTrackOperations) {
        this.bookPrintOperations = bookPrintOperations;
        this.bookTrackOperations = bookTrackOperations;
    }

    public BookHandler(BookTrackOperations bookTrackOperations) {
        this.bookTrackOperations = bookTrackOperations;
    }

    public BookHandler(BookPrintOperations bookPrintOperations) {
        this.bookPrintOperations = bookPrintOperations;
    }

    public void performPrintOperation() throws IOException {
        this.bookPrintOperations.print();
    }

    public void performTrackOperation(int pageNo) throws IOException {
        this.bookTrackOperations.track(pageNo);
    }
}

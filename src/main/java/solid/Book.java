package solid;

public class Book {
    private String title;
    private int noOfPages;
    private String author;
    private String genre;
    private String content;

    public Book(String title, int noOfPages, String author, String genre, String content) {
        this.title = title;
        this.noOfPages = noOfPages;
        this.author = author;
        this.genre = genre;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getGenre() {
        return genre;
    }

    public String getContent() {
        return content;
    }

    public int getNoOfPages() {
        return noOfPages;
    }
}

package solid;

import java.io.IOException;

/***
 * single responsibility - used to send the output to the given stream(system.out - by default)
 * Open for extension as taken from book printer super class
 *  and closed for modification(any new functionality regarding streaming/printing can be handled
 *  using a new sub class)
 */
public class BookPrinter implements BookPrintOperations {
    private Book book;

    public BookPrinter(Book book) {
        this.book = book;
    }

    public void print() throws IOException {
        String output = "\nprinting from book printer - " + this.getBook().getTitle();
        System.out.println(output);
    }

    protected Book getBook() {
        return book;
    }
}

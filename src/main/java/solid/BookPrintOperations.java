package solid;

import java.io.IOException;

/**
 * have all the book operations defined here
 */
public interface BookPrintOperations {
    void print() throws IOException;
}

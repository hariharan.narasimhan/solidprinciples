package solid;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        Book cleanCode = new Book("Clean Code", 200,
                "Robert C Martin", "Programming", "test content");
        BookPrinter cleanCodePrinter = new BookPrinter(cleanCode);
        try {
            cleanCodePrinter.print();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package solid.violation;

/**
 * violates
 * Single Responsibility Principle
 *  - has both print and track methods
 * Open for extension, closed for modification
 *  - any new functionality has to be added as a method and might use the same/modify instance variables
 *  - causing the already existing application to fail in case of modification of instance variables
 */
public class Book {
    private String title;
    private int noOfPages;
    private String author;
    private String genre;
    private String content;

    public Book(String title, int noOfPages, String author, String genre, String content) {
        this.title = title;
        this.noOfPages = noOfPages;
        this.author = author;
        this.genre = genre;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getGenre() {
        return genre;
    }

    public String getContent() {
        return content;
    }

    public int getNoOfPages() {
        return noOfPages;
    }

    // violates single responsibility - prints as well
    public void print() {
        System.out.println(this.title);
        this.track(100);
    }

    // violates single responsibility - tracks as well
    public void track(int pageNo) {
        System.out.println("current page is " + pageNo);
    }
}

package solid.violation;

/**
 * Dependency Inversion
 *  - any change happens to book, I will definitely be affected
 *  - I have to change the method signature as well if book changes its method
 *  - Even without knowing the implementations, I am forced to know the implementations of the book
 *  - any change happens to book, I have to monitor and change here as well
 */
public class BookOperations {
    private Book book;

    public BookOperations(Book book) {
        this.book = book;
    }

    public void track(int pageNo) {
        //interface_obj.track(pageNo);
        this.book.track(pageNo);
    }
}

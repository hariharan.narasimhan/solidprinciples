package solid.violation;

/**
 * Interface Segregation violation:
 *  I just want to print the book,
 *      - but I have access to track and transfer the book to any user as well
 *      - any change that is done to track or transfer can break me as well
 */
public class Printer {
    private Book book;

    public Printer(Book book) {
        this.book = book;
    }

    public void print() {
        this.book.print();
    }
}

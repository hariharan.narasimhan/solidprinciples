package solid.violation;

/**
 * Liskov Substitution
 *  - violates liskov substitution as the track method is not tracking the book
 *  - it throws an exception and does not do what the parent can do
 *  - track method will throw exception, you cannot replace this to the parent object by any way
 *  - new functionality(transfer) being added and parent has no idea about this
 */
public class BookExtender extends Book {

    public BookExtender(String title, int noOfPages, String author, String genre, String content) {
        super(title, noOfPages, author, genre, content);
    }

    @Override
    public void print() {
        super.print();
    }

    @Override
    public void track(int pageNo) {
        System.out.println("im violating the parent guy's tracking code.. here is an exception.. Have Fun");
        throw new RuntimeException("I am a child of book and I cannot follow my parent");
    }

    public void transferBookToUser(int userId) {
        // code to transfer this book to the given userId
    }
}

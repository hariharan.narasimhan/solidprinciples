package solid;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Streaming the book
 * Liskov Substitution - print() will always send the data
 * and will not behave different than its base classes print method
 */
public class BookStreamPrinter extends BookPrinter {
    OutputStream outputStream;

    public BookStreamPrinter(Book book, OutputStream outputStream) {
        super(book);
        this.outputStream = outputStream;
    }

    public void print() throws IOException {
        String output = "\nprinting from book stream printer - " + this.getBook().getTitle();
        this.outputStream.write(output.getBytes());
    }
}

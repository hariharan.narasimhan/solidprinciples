package solid;

/**
 * track the page, the current user is reading
 */
public interface BookTrackOperations {
    void track(int pageNo);
}

package solid;

import org.junit.jupiter.api.Test;

import java.io.IOException;

class BookPrinterTest {

    @Test
    void printToConsole() {
        Book cleanCode = new Book("Clean Code", 200,
                "Robert C Martin", "Programming", "test content");
        BookPrinter cleanCodePrinter = new BookPrinter(cleanCode);
        try {
            System.out.println("normal test from console");
            cleanCodePrinter.print();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    @Test
    void printToOutputStream() {
        Book cleanCode = new Book("Clean Code", 200,
                "Robert C Martin", "Programming", "test content");
        BookStreamPrinter cleanCodePrinter = new BookStreamPrinter(cleanCode, System.out);
        try {
            cleanCodePrinter.print();
        } catch (IOException e) {
           throw new AssertionError(e);
        }
    }

    /**
     * Interface Segregation
     *  bookTracker and normalHandler are classes that know nothing about each other
     *  and do two different things based on two different interfaces
     *  BookHandler manages to use both these functionalities
     *  without knowing anything about their implementations
     */
    @Test
    void printForInterfaceSegregation() {
        Book cleanCode = new Book("Clean Code", 200,
                "Robert C Martin", "Programming", "test content");
        BookPrinter normalPrinter = new BookPrinter(cleanCode);
        BookTracker bookTracker = new BookTracker(cleanCode);
        BookHandler normalHandler = new BookHandler(normalPrinter, bookTracker);
        try {
            normalHandler.performPrintOperation();
            normalHandler.performTrackOperation(20);
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    /**
     * Dependency Inversion:
     *  normal handler and stream handler belong to same class but their functionality differ
     *  from the object passed to them in constructor
     */
    @Test
    void printUsingDependencyInversion() {
        Book cleanCode = new Book("Clean Code", 200,
                "Robert C Martin", "Programming", "test content");
        BookStreamPrinter streamPrinter = new BookStreamPrinter(cleanCode, System.out);
        BookPrinter normalPrinter = new BookPrinter(cleanCode);
        BookHandler streamHandler = new BookHandler(streamPrinter);
        BookHandler normalHandler = new BookHandler(normalPrinter);
        try {
            streamHandler.performPrintOperation();
            normalHandler.performPrintOperation();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }
}

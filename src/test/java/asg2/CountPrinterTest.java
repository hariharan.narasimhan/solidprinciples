package asg2;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

class CountPrinterTest {
    @Test
    void generateCounterFile() {
        String inputFilePath = "/Users/harin/IdeaProjects/Learn/src/test/java/asg2/inputFile.txt";
        CharacterCounter characterCounter = new CharacterCounter(inputFilePath);
        String outputFilePath = "/Users/harin/IdeaProjects/Learn/src/test/java/asg2/output.txt";
        String expectedOutputFilePath = "/Users/harin/IdeaProjects/Learn/src/test/java/asg2/expectedOutput.txt";
        CountPrinter countPrinter = new CountPrinter(characterCounter, outputFilePath);
        try {
            characterCounter.countCharacters();
            countPrinter.writeToFile();

            File outFile = new File(outputFilePath);
            Scanner outputFileScanner = new Scanner(outFile);

            File expectedOutFile = new File(expectedOutputFilePath);
            Scanner expectedOutputFileScanner = new Scanner(expectedOutFile);

            while(expectedOutputFileScanner.hasNext()) {
                String expected = expectedOutputFileScanner.nextLine();
                String output = outputFileScanner.nextLine();
                Assertions.assertEquals(expected, output);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
package asg1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SListTest {

    @Test
    public void listAdditionTest() {
        SList<String> sList = new SList<>();
        SListIterator<String> sListIterator = sList.iterator();
        sListIterator.add("first string");
        sListIterator.add("second string");
        sListIterator.add("third string");
        sListIterator.add("four string");
        String expected = "\n" +
                "first string,\n" +
                "second string,\n" +
                "third string,\n" +
                "four string,";
        Assertions.assertEquals(expected, sList.toString());
    }
}